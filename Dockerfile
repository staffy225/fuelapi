FROM python:3.7

RUN pip install fastapi uvicorn sqlalchemy mysql-connector-python python-multipart pydantic

COPY ./api /api/api

ENV PYTHONPATH=/api
WORKDIR /api

EXPOSE 8000

ENTRYPOINT ["uvicorn"]
CMD ["api.main:app", "--host", "0.0.0.0"] 

