# database model

from sqlalchemy.schema import Column

from sqlalchemy.types import String, Integer, Text

from api.database import Base


class Fuel(Base):
    __tablename__ = "Fuels"
    id = Column(Integer, primary_key=True, index=True)
    montant = Column(Integer)
    date = Column(String(50))
    lon = Column(String(30))
    lat = Column(String(30))
    previous_km = Column(Integer)
    station_name = Column(String(30))
    ville = Column(String(50))