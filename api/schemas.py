
from datetime import datetime

from pydantic import BaseModel



class Fuel(BaseModel):
    id = int
    montant = int
    date = str
    lon = str
    lat = str
    previous_km = int
    station_name = str 
    ville = str 

    class Config:
        orm_mode = True