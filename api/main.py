from typing import Union, Optional, List

from fastapi import FastAPI, HTTPException, Form, Depends, Request
from sqlalchemy.orm import Session

from fastapi.responses import HTMLResponse

from pydantic import BaseModel
from . import schemas, models

from api.database import SessionLocal, engine


app = FastAPI(title="Fuel Manager", version=" V1")


models.Base.metadata.create_all(bind=engine)



store_fuel= [

]

def get_database_session():
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()

@app.get("/")
async def home():
    return {"Message": "Welcome to Fuel Manager"}


# @app.get("/items/",response_model=List[Fuel])
# async def get_all_items():
#     return store_fuel


# @app.get("/items/{id}")
# async def get_item(id: int):
#     try:
#         return store_fuel[id]
#     except:
#         raise HTTPException(status_code=404,detail="Fuel not found in databases")


# @app.post("/item/")
# async def create(fuel: Fuel):
#     store_fuel.append(fuel)
#     return fuel


# @app.put("/item/{id}")
# async def update_fuel(id: int, new_fuel: Fuel):
#     try:
#         store_fuel[id]= new_fuel
#         return store_fuel[id]
#     except:
#         raise HTTPException(status_code=404,detail="Fuel not updated")



# @app.delete("/item/{id}")
# async def delete_fuel(id: int):
#     try:
#         obj = store_fuel[id]
#         store_fuel.pop[id]
#         return obj 
#     except:
#         raise HTTPException(status_code=404,detail="Fuel not updated")




@app.post("/login/")
async def login(username: str=Form(...), password: str= Form(...)):
    return{username: username}


@app.post("/create/")
async def create_fuel(db: Session = Depends(get_database_session), montant: schemas.Fuel.montant = Form(...), date: schemas.Fuel.date = Form(...), lon: schemas.Fuel.lon = Form(...), lat: schemas.Fuel.lat = Form(...), previous_km: schemas.Fuel.previous_km = Form(...),station_name: schemas.Fuel.station_name = Form(...), ville: schemas.Fuel.ville = Form(...)):
    fuel = models.Fuel(montant=montant, date=date, lon=lon, lat=lat, previous_km=previous_km, station_name = station_name, ville = ville)
    db.add(fuel)
    db.commit()
    #response = RedirectResponse('/', status_code=303)
    return fuel

@app.get("/movie", response_class=HTMLResponse)
async def read_movies(request: Request, db: Session = Depends(get_database_session)):
    records = db.query(Fuel).all()
    return records

@app.get("/movie/{id}", response_class=HTMLResponse)
def read_movie(request: Request, name: schemas.Fuel.montant, db: Session = Depends(get_database_session)):
    item = db.query(Fuel).filter(Fuel.id==montant).first()
    print(item)
    return templates.TemplateResponse("overview.html", {"request": request, "movie": item})


@app.patch("/movie/{id}")
async def update_movie(request: Request, id: int, db: Session = Depends(get_database_session)):
    requestBody = await request.json()
    movie = db.query(Movie).get(id)
    movie.name = requestBody['name']
    movie.desc = requestBody['desc']
    db.commit()
    db.refresh(movie)
    newMovie = jsonable_encoder(movie)
    return JSONResponse(status_code=200, content={
        "status_code": 200,
        "message": "success",
        "movie": newMovie
    })


@app.delete("/movie/{id}")
async def delete_movie(request: Request, id: int, db: Session = Depends(get_database_session)):
    movie = db.query(Movie).get(id)
    db.delete(movie)
    db.commit()
    return JSONResponse(status_code=200, content={
        "status_code": 200,
        "message": "success",
        "movie": None
    })

