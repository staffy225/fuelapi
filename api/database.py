from sqlalchemy import create_engine

import os


from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.orm import sessionmaker

HOST = os.environ.get('MYSQL_HOST')
DATABASE_URL = "mysql+mysqlconnector://root@aae6f7688e02:3306/fuel"
# SQLALCHEMY_DATABASE_URL = "sqlite:///./sql_app.db"
# SQLALCHEMY_DATABASE_URL = "postgresql://user:password@postgresserver/db"

engine = create_engine(DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base = declarative_base() 