# fuelapi

fuel api is an api set up to just control the expenses related to fuel. Every time you go to the gas station through your Apple device like Iphone, Apple Watch or Mac, you can record an expense using the shortcut app. By clicking on the Fuel shortcut, you can enter the amount and choose from the list of station names available, it will take care of recording on the server, the amount, the location. For later use, you can use the data collected to draw up a table of fuel-related expenses. I did it just to see how much I spend on fuel per day, per month and for how many kilometers

it only works with apple and shortcut

for server api you can use docker with fastapi and mysql for parsing and storage
